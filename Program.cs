﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] inna;
            byte[,][] initialKey = new byte[2,2][] {
                { new byte[4] { 1,0,1,1 }, new byte[4] { 0,0,1,0 } }, 
                { new byte[4] { 1,1,1,1 }, new byte[4] { 0,1,1,0 } }
            };

            inna = MatrixMath.SingleMultiply(initialKey[0,0], initialKey[1,0]);
            //Console.WriteLine(zeroKey[0,0][0]);
            for (int i = 0; i < 4; i++)
                Console.Write(inna[i]);

            Console.ReadKey();
        }
    }
}