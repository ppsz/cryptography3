﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    public static class MatrixMath
    {
        //Dodawanie dwoch macierzy
        public static byte[,][] Add(byte[,][] Augend, byte[,][] Addend)
        {
            byte[,][] sum = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] }
            };

            for (byte i = 0; i < 2; i++)
                for (var j = 0; j < 2; j++)
                    for (var k = 0; k < 4; k++)
                        sum[i, j][k] = (byte)((Augend[i, j][k] ^ Addend[i, j][k]));

            return sum;
        }
        //Mnozenie dwoch macierzy
        public static byte[,][] Multiply(byte[,][] Multiplier, byte[,][] Multiplicand)
        {
            byte[,][] product = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] }
            };

            return product;
        }
        //Mnozenie dwoch czterobitowych tablic, PRIVATE PO TESTACH!
        public static byte[] SingleMultiply(byte[] Multiplier, byte[] Multiplicand)
        {
            byte[] product = new byte[4];
            byte[] indirectProduct = new byte[7];
            byte[,] tempArray = new byte[4,7];
            byte[] test = new byte[7] { 0, 1, 0, 1, 0, 1, 0 };
            byte[,] polynomial = new byte[3, 7] { 
            { 1, 0, 0, 1, 1, 0, 0 }, { 0, 1, 0, 0, 1, 1, 0 }, { 0, 0, 1, 0, 0, 1, 1 } 
            };

            for (int i =0; i<4; i++)
                for(int j = 0;j<4;j++)
                    if (Multiplicand[i] == 1)
                        tempArray[i, j + i] = Multiplier[j];
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 7; j++)
                    indirectProduct[j] ^= tempArray[i, j];
            
            //for (int i = 0; i < 3; i++)
            //    if (indirectProduct[i] == 1)
            //        for (int j = 0; j < 7; j++)
            //           indirectProduct[j] ^= polynomial[i, j];
            //for (int i = 0; i < 4; i++)
            //    product[i] = indirectProduct[i + 3];

            for (int i = 0; i < 3; i++)
                if (test[i] == 1)
                    for (int j = 0; j < 7; j++)
                        test[j] ^= polynomial[i, j];
            for (int i = 0; i < 4; i++)
                product[i] = test[i + 3];

            //---test
            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //    {
            //        Console.Write(tempArray[i, j]);
            //    }
            //    Console.WriteLine();
            //}
            //Console.WriteLine("\n");
            //for (int i = 0; i < 7; i++)
            //    Console.Write(indirectProduct[i]);

            return product;
        }
    }
}