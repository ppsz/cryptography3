﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    public static class Sbox
    {
        private static Dictionary<string, byte[]> SBoxE = new Dictionary<string, byte[]>() {
            { "0,0,0,0", new byte[] {1,1,1,0}},
            { "0,0,0,1", new byte[] {0,1,0,0}},
            { "0,0,1,0", new byte[] {1,1,0,1}},
            { "0,0,1,1", new byte[] {0,0,0,1}},
            { "0,1,0,0", new byte[] {0,0,1,0}},
            { "0,1,0,1", new byte[] {1,1,1,1}},
            { "0,1,1,0", new byte[] {1,0,1,1}},
            { "0,1,1,1", new byte[] {1,0,0,0}},
            { "1,0,0,0", new byte[] {0,0,1,1}},
            { "1,0,0,1", new byte[] {1,0,1,0}},
            { "1,0,1,0", new byte[] {0,1,1,0}},
            { "1,0,1,1", new byte[] {1,1,0,0}},
            { "1,1,0,0", new byte[] {0,1,0,1}},
            { "1,1,0,1", new byte[] {1,0,0,1}},
            { "1,1,1,0", new byte[] {0,0,0,0}},
            { "1,1,1,1", new byte[] {0,1,1,1}}
            /*{ new byte[] {0,0,0,0}, new byte[] {1,1,1,0}},
            { new byte[] {0,0,0,1}, new byte[] {0,1,0,0}},
            { new byte[] {0,0,1,0}, new byte[] {1,1,0,1}},
            { new byte[] {0,0,1,1}, new byte[] {0,0,0,1}},
            { new byte[] {0,1,0,0}, new byte[] {0,0,1,0}},
            { new byte[] {0,1,0,1}, new byte[] {1,1,1,1}},
            { new byte[] {0,1,1,0}, new byte[] {1,0,1,1}},
            { new byte[] {0,1,1,1}, new byte[] {1,0,0,0}},
            { new byte[] {1,0,0,0}, new byte[] {0,0,1,1}},
            { new byte[] {1,0,0,1}, new byte[] {1,0,1,0}},
            { new byte[] {1,0,1,0}, new byte[] {0,1,1,0}},
            { new byte[] {1,0,1,1}, new byte[] {1,1,0,0}},
            { new byte[] {1,1,0,0}, new byte[] {0,1,0,1}},
            { new byte[] {1,1,0,1}, new byte[] {1,0,0,1}},
            { new byte[] {1,1,1,0}, new byte[] {0,0,0,0}},
            { new byte[] {1,1,1,1}, new byte[] {0,1,1,1}}*/
        };
        private static Dictionary<string, byte[]> SBoxD = new Dictionary<string, byte[]>() {
            { "0,0,0,0", new byte[] {1,1,1,0}},
            { "0,0,0,1", new byte[] {0,0,1,1}},
            { "0,0,1,0", new byte[] {0,1,0,0}},
            { "0,0,1,1", new byte[] {1,0,0,0}},
            { "0,1,0,0", new byte[] {0,0,0,1}},
            { "0,1,0,1", new byte[] {1,1,0,0}},
            { "0,1,1,0", new byte[] {1,0,1,0}},
            { "0,1,1,1", new byte[] {1,1,1,1}},
            { "1,0,0,0", new byte[] {0,1,1,1}},
            { "1,0,0,1", new byte[] {1,1,0,1}},
            { "1,0,1,0", new byte[] {1,0,0,1}},
            { "1,0,1,1", new byte[] {0,1,1,0}},
            { "1,1,0,0", new byte[] {1,0,1,1}},
            { "1,1,0,1", new byte[] {0,0,1,0}},
            { "1,1,1,0", new byte[] {0,0,0,0}},
            { "1,1,1,1", new byte[] {0,1,0,1}}
            /*{ new byte[] {0,0,0,0}, new byte[] {1,1,1,0}},
            { new byte[] {0,0,0,1}, new byte[] {0,0,1,1}},
            { new byte[] {0,0,1,0}, new byte[] {0,1,0,0}},
            { new byte[] {0,0,1,1}, new byte[] {1,0,0,0}},
            { new byte[] {0,1,0,0}, new byte[] {0,0,0,1}},
            { new byte[] {0,1,0,1}, new byte[] {1,1,0,0}},
            { new byte[] {0,1,1,0}, new byte[] {1,0,1,0}},
            { new byte[] {0,1,1,1}, new byte[] {1,1,1,1}},
            { new byte[] {1,0,0,0}, new byte[] {0,1,1,1}},
            { new byte[] {1,0,0,1}, new byte[] {1,1,0,1}},
            { new byte[] {1,0,1,0}, new byte[] {1,0,0,1}},
            { new byte[] {1,0,1,1}, new byte[] {0,1,1,0}},
            { new byte[] {1,1,0,0}, new byte[] {1,0,1,1}},
            { new byte[] {1,1,0,1}, new byte[] {0,0,1,0}},
            { new byte[] {1,1,1,0}, new byte[] {0,0,0,0}},
            { new byte[] {1,1,1,1}, new byte[] {0,1,0,1}}*/
        };
        public static byte[] E(byte[] Input)
        {
            string key = string.Join(",", Input);
            byte[] result = new byte[4];
            //SBoxE.TryGetValue(Input, out result);
            result = SBoxE[key];

            return result;
        }
        public static byte[] D(byte[] Input)
        {
            string key = string.Join(",", Input);
            byte[] result = new byte[4];
            //SBoxD.TryGetValue(Input, out result);
            result = SBoxD[key];

            return result;
        }
        public static byte[,][] Fsbox(byte[,][] Input, string EncryptDecrypt)
        {
            byte[,][] result = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };

            for (int i = 0; i< 2; i++)
                for (int j = 0; j< 2; j++)
                {
                    if (EncryptDecrypt == "E")
                        result[i, j] = E(Input[i, j]);
                    else if (EncryptDecrypt == "D")
                        result[i, j] = D(Input[i, j]);
                }
                    

            return result;
        }
    }
}
