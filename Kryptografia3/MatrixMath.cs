﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    public static class MatrixMath
    {
        //Dodawanie dwoch macierzy
        public static byte[,][] Add(byte[,][] Augend, byte[,][] Addend)
        {
            byte[,][] sum = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };

            for (byte i = 0; i < 2; i++)
                for (var j = 0; j < 2; j++)
                    for (var k = 0; k < 4; k++)
                        sum[i, j][k] = (byte)((Augend[i, j][k] ^ Addend[i, j][k]));

            return sum;
        }
        //Dodawanie dwoch elementow macierzy
        public static byte[] AddElement(byte[] Augend, byte[] Addend)
        {
            byte[] sum = new byte[4];

            for (int i = 0; i < 4; i++)
                sum[i] = (byte)(Augend[i] ^ Addend[i]);

            return sum;
        }
        //Dodawanie dwoch liczb 7 bitowych
        private static byte[] AddBigElement(byte[] Augend, byte[] Addend)
        {
            byte[] sum = new byte[7];

            for (int i = 0; i < 7; i++)
                sum[i] = (byte)(Augend[i] ^ Addend[i]);

            return sum;
        }
        //Mnozenie dwoch macierzy
        public static byte[,][] Multiply(byte[,][] Multiplier, byte[,][] Multiplicand)
        {
            byte[,] polynomial = new byte[3, 7] { { 1, 0, 0, 1, 1, 0, 0 }, { 0, 1, 0, 0, 1, 1, 0 }, { 0, 0, 1, 0, 0, 1, 1 } };
            byte[] tempProduct = new byte[7];
            byte[,][] product = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };

            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                {
                    tempProduct = AddBigElement(SingleMultiply(Multiplier[i, 0], Multiplicand[0, j]), SingleMultiply(Multiplier[i, 1], Multiplicand[1, j]));
                    for (int k = 0; k < 3; k++)
                        if (tempProduct[k] == 1)
                            for (int m = 0; m < 7; m++)
                                tempProduct[m] ^= polynomial[k, m];
                        else
                            continue;
                    for (int n = 0; n < 4; n++)
                        product[i,j][n] = tempProduct[n + 3];
                }


            return product;
        }
        //Mnozenie dwoch czterobitowych tablic
        private static byte[] SingleMultiply(byte[] Multiplier, byte[] Multiplicand)
        {
            //byte[] product = new byte[4];
            byte[] product = new byte[7];
            byte[,] tempArray = new byte[4, 7];

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    if (Multiplicand[i] == 1)
                        tempArray[i, j + i] = Multiplier[j];
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 7; j++)
                    product[j] ^= tempArray[i, j];

            //---test
            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //    {
            //        Console.Write(tempArray[i, j]);
            //    }
            //    Console.WriteLine();
            //}
            //for (int i = 0; i < 7; i++)
            //    Console.Write(indirectProduct[i]);
            //Console.WriteLine();

            return product;
        }
        public static byte[,][] ParseToMatrix(byte[] Input)
        {
            byte[,][] result = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };
            for (int i = 0, m = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 4; k++, m++)
                        result[i, j][k] = Input[m];

            return result;
        }
    }
}
