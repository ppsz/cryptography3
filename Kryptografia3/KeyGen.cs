﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    public static class Key
    {
        public static byte[,][] Generate(byte KeyNumber, byte[,][] PreviousKey)
        {
            byte[,][] result = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };
            byte[] firstConst = new byte[] { 0, 0, 0, 1 };
            byte[] secondConst = new byte[] { 0, 0, 1, 0 };
            if (KeyNumber == 1)
            {
                result[0, 0] = MatrixMath.AddElement(PreviousKey[0, 0], Sbox.E(PreviousKey[1, 1]));
                result[0, 0] = MatrixMath.AddElement(result[0, 0], firstConst);
                result[1, 0] = MatrixMath.AddElement(PreviousKey[1, 0], result[0, 0]);
                result[0, 1] = MatrixMath.AddElement(PreviousKey[0, 1], result[1, 0]);
                result[1, 1] = MatrixMath.AddElement(PreviousKey[1, 1], result[0, 1]);
            }
            else if (KeyNumber == 2)
            {
                result[0, 0] = MatrixMath.AddElement(PreviousKey[0, 0], Sbox.E(PreviousKey[1, 1]));
                result[0, 0] = MatrixMath.AddElement(result[0, 0], secondConst);
                result[1, 0] = MatrixMath.AddElement(PreviousKey[1, 0], result[0, 0]);
                result[0, 1] = MatrixMath.AddElement(PreviousKey[0, 1], result[1, 0]);
                result[1, 1] = MatrixMath.AddElement(PreviousKey[1, 1], result[0, 1]);
            }
            else
            {
                Console.WriteLine("You can generate only first or second round key");
            }

            return result;
        }

        public static byte[,][] ZK(byte[,][] Input)
        {

            byte[] temp = new byte[4];
            temp = Input[1,0];
            Input[1, 0] = Input[1, 1];
            Input[1, 1] = temp;

            return Input;
        }

        public static byte[,][] MM(byte[,][] Input)
        {
            byte[,][] result = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };
            byte[,][] mConstant = new byte[2, 2][] {
                { new byte[4] { 0,0,1,1 }, new byte[4] { 0,0,1,0 } },
                { new byte[4] { 0,0,1,0 }, new byte[4] { 0,0,1,1 } } };

            result = MatrixMath.Multiply(mConstant, Input);

            return result;
        }
    }
}
