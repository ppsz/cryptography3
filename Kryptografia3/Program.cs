﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kryptografia3
{
    class Program
    {
        static void Main(string[] args)

        {
            byte[] message = { 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1 };
            byte[] initialKey = { 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0 };
            byte[,][] firstRoundKey = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };
            byte[,][] secondRoundKey = new byte[2, 2][] {
                { new byte[4], new byte[4] },
                { new byte[4], new byte[4] } };
            byte[,][] messageMatrix = MatrixMath.ParseToMatrix(message);
            byte[,][] initialKeyMatrix = MatrixMath.ParseToMatrix(initialKey);
            string option;

            do
            {
                Console.WriteLine("Szyfrowanie: 1\nOdszyfrowanie: 2\nWyjscie: q");
                option = Console.ReadLine();
                Console.Clear();
                firstRoundKey = Key.Generate(1, initialKeyMatrix);
                secondRoundKey = Key.Generate(2, firstRoundKey);
                if (option == "1")
                {
                    messageMatrix = MatrixMath.Add(messageMatrix, initialKeyMatrix);
                    messageMatrix = Sbox.Fsbox(messageMatrix, "E");
                    messageMatrix = Key.ZK(messageMatrix);
                    messageMatrix = Key.MM(messageMatrix);
                    messageMatrix = MatrixMath.Add(messageMatrix, firstRoundKey);
                    messageMatrix = Sbox.Fsbox(messageMatrix, "E");
                    messageMatrix = Key.ZK(messageMatrix);
                    messageMatrix = MatrixMath.Add(messageMatrix, secondRoundKey);
                    Console.WriteLine("Wynik szyfrowania:");
                    for (int i = 0; i < 2; i++)
                        for (int j = 0; j < 2; j++)
                            for (int k = 0; k < 4; k++)
                                Console.Write(messageMatrix[i, j][k]);
                    Console.WriteLine("\n");
                }
                else if (option == "2")
                {
                    messageMatrix = MatrixMath.Add(messageMatrix, secondRoundKey);
                    messageMatrix = Key.ZK(messageMatrix);
                    messageMatrix = Sbox.Fsbox(messageMatrix, "D");
                    messageMatrix = MatrixMath.Add(messageMatrix, firstRoundKey);
                    messageMatrix = Key.MM(messageMatrix);
                    messageMatrix = Key.ZK(messageMatrix);
                    messageMatrix = Sbox.Fsbox(messageMatrix, "D");
                    messageMatrix = MatrixMath.Add(messageMatrix, initialKeyMatrix);
                    Console.WriteLine("Wynik odszyfrowania:");
                    for (int i = 0; i < 2; i++)
                        for (int j = 0; j < 2; j++)
                            for (int k = 0; k < 4; k++)
                                Console.Write(messageMatrix[i, j][k]);
                    Console.WriteLine("\n");
                }
            }
            while (option != "q");
        }
    }
}
